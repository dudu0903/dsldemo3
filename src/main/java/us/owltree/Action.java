package us.owltree;

public enum Action {
    Restrict, Append, Override
}
