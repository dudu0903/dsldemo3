package us.owltree;

import java.util.List;

public interface DslManagerInterface {
    public List<Worker> runTask(Task task);
    public boolean runTestCases();
    public List<Worker> runInlineScript(String script, List<Worker> workerList);
}
