package us.owltree;

import groovy.util.ResourceException;
import org.codehaus.groovy.runtime.IOGroovyMethods;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

public class HyPelToGroovyTranslator {

    public static String loadGroovyFromHyPELFile(String fileName) throws ResourceException {
        String content = loadUrlFileByName(fileName);

        String groovy = translateHyPELtoGroovy(content);

        return groovy;
    }

    public static String translateHyPELtoGroovy(String hyPELScript) {
        //policy "restrict for Render" when task.name contains "render" then Action.Restrict to worker.name contains "render"
        //dslManager.policy "restrict for Render" when {Task task, Worker worker -> return task.name.contains("render")} then Action.Restrict to {Worker worker -> return worker.name.contains("render")}

        String header = "import us.owltree.*\n" +
                "import groovy.*\n" +
                "\n" +
                "def dslManager = new DslManager()\n" +
                "def them = \"them\"\n";

        StringBuilder sb = new StringBuilder(header);

        String[] hyPELScriptCommands = hyPELScript.split("policy");

        if(hyPELScriptCommands.length > 0) {
            Arrays.stream(hyPELScriptCommands)
                    .filter(s -> !s.isEmpty())
                    .forEach(command -> {
                        //dslManager.
                        String modifiedCommand = "\npolicy " + modifiedCommand(command);
                        sb.append(modifiedCommand.replace("policy", "dslManager.policy") );
                    });
        }

        sb.append("\nreturn dslManager");
        return sb.toString();
    }

    public static String modifiedCommand(String command) {
        String modifiedCommand = command;

        //dslManager.policy "restrict for Render" when {Task task, Worker worker -> return task.name.contains("render")} then Action.Restrict to {Worker worker -> return worker.name.contains("render")}

        modifiedCommand = modifiedCommand.replaceAll("^(\\s+)//(.*)$", "//$2");

        //remove new line character.
        modifiedCommand = modifiedCommand.replace("\n", " ");

        //convert (something) to ( something ) to make parsing easy.
        modifiedCommand = modifiedCommand.replaceAll("(\\()([^\\(\\)]+)(\\))", "( $2 )");

        //contains "something" -> .contains("something")
        modifiedCommand = modifiedCommand.replaceAll("(\\s+)(contains|startsWith|endsWith)(\\s+)\"([^\"]+)\"(\\s+)", ".$2(\"$4\") ");

        //policy -> \npolicy
        modifiedCommand = modifiedCommand.replaceAll("policy", "\npolicy");

        //when something then -> when {Task task, Worker worker -> return something } then
        modifiedCommand = modifiedCommand.replaceAll("(\\s+)when (.*)\\)(\\s+)then ", " when {Task task, Worker worker -> return $2\\)} then ");

        //then Action.Restrict to something -> then Action.Restrict to {Worker worker -> return $3}
        modifiedCommand = modifiedCommand.replaceAll(" then(\\s+)Action.Restrict(\\s+)to(.*)", " then Action.Restrict to {Worker worker -> return $3}");

        //and
        modifiedCommand = modifiedCommand.replaceAll("\\)(\\s+)and(\\s+)", ") && ");
        //or
        modifiedCommand = modifiedCommand.replaceAll("\\)(\\s+)or(\\s+)", ") || ");

        return modifiedCommand;
    }

    public static String loadUrlFileByName(String scriptName) throws ResourceException {
        URLConnection conn = getResourceConnection(scriptName);
        String content = null;

        try {
            String encoding = conn.getContentEncoding() != null ? conn.getContentEncoding() : "US-ASCII";
            content = IOGroovyMethods.getText(conn.getInputStream(), encoding);

        } catch (IOException var11) {
            throw new ResourceException(var11);
        } finally {
            forceClose(conn);
        }

        return content;
    }

    public static URLConnection getResourceConnection(String resourceName) throws ResourceException {

        URLConnection groovyScriptConn = null;
        ResourceException se = null;

        try {
            groovyScriptConn = openConnection(new URL(resourceName));
        } catch (MalformedURLException e) {
            se = new ResourceException("Malformed URL: " + resourceName);
        } catch (IOException e) {
            se = new ResourceException("Cannot open URL: " + resourceName);
        }

        if (groovyScriptConn == null) {

            if (se == null) {
                se = new ResourceException("No resource for " + resourceName + " was found");
            }

            throw se;
        } else {
            return groovyScriptConn;
        }
    }

    private static URLConnection openConnection(URL scriptURL) throws IOException {
        URLConnection urlConnection = scriptURL.openConnection();
        verifyInputStream(urlConnection);
        return scriptURL.openConnection();
    }

    private static void verifyInputStream(URLConnection urlConnection) throws IOException {
        InputStream in = null;

        try {
            in = urlConnection.getInputStream();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignore) {}
            }
        }
    }

    private static void forceClose(URLConnection urlConnection) {
        if (urlConnection != null) {
            try {
                verifyInputStream(urlConnection);
            } catch (Exception ignore) {}
        }
    }
}
