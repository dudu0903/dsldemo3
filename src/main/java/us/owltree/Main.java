package us.owltree;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import groovy.util.GroovyScriptEngine;
import org.codehaus.groovy.control.CompilerConfiguration;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        CompilerConfiguration config = new CompilerConfiguration();
        //config.setScriptBaseClass(DslManagerInterface.class.getName());
/*
        GroovyShell shell = new GroovyShell(new Binding(), config);
        Script script = shell.parse("greet()");
        script.run();*/


        try {
            Main main = new Main();
            GroovyScriptEngine engine = new GroovyScriptEngine(new URL[]{main.getClass().getResource("")});
            //engine.setConfig(config);
            Binding binding = new Binding();

            DslManagerInterface currentDslManager = null;
            while (true) {
                try {
                    Object obj = engine.run("../../policies.HyPEL", binding);

                    //GroovyShell shell = new GroovyShell(new Binding(), config);
                    //Script script = shell.parse("greet()");
                    //script.run();

                    System.out.println(obj.getClass().getName());

                    if (obj instanceof DslManagerInterface) {
                        if (runTasks((DslManagerInterface) obj)) {
                            currentDslManager = (DslManagerInterface) obj;
                        } else {
                            System.out.println("************* error. recover it to prev settings ****************");
                            runTasks(currentDslManager);
                        }
                    }
                } catch (Exception e) {
                    System.out.println("error in HyPEL.");
                    e.printStackTrace();
                }



                Thread.sleep(5000);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean runTasks(DslManagerInterface dslManagerInterface) {

        if(dslManagerInterface == null) {
            return false;
        }

        try {
            if(!dslManagerInterface.runTestCases()) {
                return false;
            }

            List<Worker> workerList = dslManagerInterface.runTask(new Task("scp", "s1"));
            System.out.println(Arrays.toString(workerList.toArray()));

            workerList = dslManagerInterface.runTask(new Task("render", "s1"));
            System.out.println(Arrays.toString(workerList.toArray()));

            workerList = dslManagerInterface.runTask(new Task("reboot", "s1"));
            System.out.println(Arrays.toString(workerList.toArray()));

            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
