package us.owltree;

public class Requirement {
    public String name;
    public String value;

    public Requirement(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return name+ " : " + value ;
    }
}
