package us.owltree;

import java.util.ArrayList;
import java.util.List;

public class Task {
    public String name;
    public String id;
    public List<Requirement> requirementList = new ArrayList<>();

    public Task(String name, String id) {
        this.name = name;
        this.id = id;
        requirementList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "["+ name +" : " + id + " with "+ requirementList +"]";
    }
}
