package us.owltree;

public class Worker {
    public String name;
    public String id;

    public Worker(String name, String id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return name +" : " + id;
    }
}
