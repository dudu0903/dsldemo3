package groovy

import us.owltree.*
import java.util.stream.Collectors

class DslManager extends Script implements DslManagerInterface {

    String format(Integer i) {
        i.toString()
    }

    def storedWorkerList = [new Worker("scp-worker1","w1"), new Worker("render-worker2","w2"), new Worker("manager-worker3","w3"), new Worker("scp-worker4","w4")]
    def policyList =[]

    static void main(String[] args) {
        def app = new groovy.DslManager()

        /*
        app.runTask(new Task("scp", "t1"))
        app.addPolicyScp()
        app.addPolicyOverride()
*/

        app.policyList.clear()
        app.addPolicyRender()
        app.addPolicyScp()
        app.addPolicyOverride()

        app.runTask(new Task("render", "t2"))

        app.runTask(new Task("scp", "t2"))

        app.runTask(new Task("reboot", "t2"))

    }

    def printStatusStart() {
        System.out.println("-----------------------------------")
        System.out.println(filteredWorkerList+"| task:" + task)
    }

    def printStatusEnd() {
        System.out.println(filteredWorkerList+"| task:" + task)
    }

    def addPolicyScp() {
        policy "restrict for Scp" when { Task task, Worker worker -> return ((task.name.contains("scp") && worker.name.startsWith("scp")) || (!task.name.contains("scp") && !worker.name.startsWith("scp")) ) } then Action.Restrict it
    }

    def addPolicyRender() {
        policy "restrict for Render" when { Task task, Worker worker -> return task.name.contains("render")} then Action.Restrict to { Worker worker -> return worker.name.contains("render")}
    }

    def addPolicyOverride() {
        policy "override requirements for rendering" when { Task task, Worker worker -> return task.name.contains("rendering") && task.name.contains("duplo")} then Action.Override requirements(["cpu":"5", "memory":"128"])
    }

    def policy(String name) {
        [when:{ condition ->
            [then: { action ->
                if(action == Action.Restrict) {
                    [to: { obj ->
                        if(obj instanceof Closure) {
                            System.out.println(((Action)action).name() + name)

                            policyList.add({task, workerList ->
                                if(condition(task, null)) {
                                    Closure closure = obj
                                    return workerList.stream().filter({w -> closure(w)}).collect(Collectors.toList())
                                } else {
                                    //do nothing
                                    return workerList
                                }
                            })
                        } else if(obj instanceof String) {
                            System.out.println(((Action)action).name() + name)
                            policyList.add({task, workerList -> return workerList.stream().filter({w -> condition(task, w)}).collect(Collectors.toList())})
                        } else {
                            System.out.println("[ignored] "+action + "," + obj)
                        }
                    }]
                } else if(action == Action.Override) {
                    [requirements: { map ->
                        System.out.println(((Action)action).name() + name)

                        policyList.add({task, workerList ->

                                for(Worker worker:workerList) {
                                    if(condition(task, worker)) {
                                        ((Map<String, String>)map)
                                                .entrySet()
                                                .stream()
                                                .forEach({ entry ->
                                            task.requirementList.add(new Requirement(entry.key, entry.value))
                                        })
                                    }
                                }
                                return workerList
                            })
                    }]
                }
            }]
        }]
    }

    @Override
    Object run() {
        return null
    }

    @Override
    List<Worker> runTask(Task task, workerList = storedWorkerList) {
        System.out.println("\n\n-----------------------------------")
        System.out.println(task.name)
        System.out.println("-----------------------------------")

        def filteredWorkerList = workerList
        for(Closure closure:policyList) {
            filteredWorkerList = closure(task, filteredWorkerList)
            System.out.println(filteredWorkerList)
        }

        return filteredWorkerList;
    }


    @Override
    boolean runTestCases() {


        def scpWorker = new Worker("scp-worker1","w1")
        def renderWorker = new Worker("render-worker2","w2")
        def storedWorkerList = [scpWorker, renderWorker]

        def filteredWorkerList = runTask(new Task("scp","t1"), storedWorkerList)
        if(filteredWorkerList.get(0) != scpWorker ) {
            return false
        }

        filteredWorkerList = runTask(new Task("render","t1"), storedWorkerList)
        if(filteredWorkerList.get(0) != renderWorker ) {
            return false
        }

        return true
    }

    List<Worker> runInlineScript(String script, List<Worker> workerList) {
        System.out.println(evaluate("2+3"))

        def scpWorker = new Worker("scp-worker1","w1")
        def renderWorker = new Worker("render-worker2","w2")
        def storedWorkerList = [scpWorker, renderWorker]

        return storedWorkerList
    }
}