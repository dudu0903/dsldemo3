package us.owltree;

import groovy.util.ResourceException;
import org.junit.Test;

import java.net.URL;

public class HyPelToGroovyTranslatorTest {

    @Test
    public void testHyPELModifier() throws ResourceException {
        String command = "policy \"restrict for Render\" when task.name contains \"render then \" then Action.Restrict to worker.name contains \" then \"\n" +
                "\n" +
                "policy \"restrict for Render\"\n" +
                " when task.name contains \"render\"\n" +
                " then Action.Restrict to worker.name contains \"render\"\n" +
                "\n" +
                "policy 'restrict for Scp'\n" +
                " when (task.name contains \"scp\" and worker.name startsWith \"scp\")\n" +
                "   or (!task.name contains \"scp\" and !worker.name startsWith \"scp\")\n" +
                "then Action.Restrict it\n";

        String command1 = "policy \"restrict for Render\" when task.name contains \"render\" then Action.Restrict to worker.name contains \"render\"\n" +
                "policy \"override requirements for rendering\" when task.name contains \"rendering\" and task.name contains \"duplo\" then Action.Override requirements([\"cpu\":\"5\", \"memory\":\"128\"])\n" +
                "policy 'restrict for Scp' when ((task.name.contains(\"scp\") and worker.name.startsWith(\"scp\")) or (!task.name.contains(\"scp\") and !worker.name.startsWith(\"scp\"))) then Action.Restrict it\n" +
                "policy 'restrict for Scp^^' when (((task.name.contains(\"scp\") and worker.name.startsWith(\"scp\")) or (!task.name.contains(\"scp\") and !worker.name.startsWith(\"scp\")))) then Action.Restrict to \"them\"";

        String output = HyPelToGroovyTranslator.translateHyPELtoGroovy(command1);
        System.out.print(output);

        //URL url = HyPelToGroovyTranslatorTest.class.getResource("policies.HyPEL");
        //String url = "/Users/duhyunkim/git/dsldemo3/target/classes/policies.HyPEL";
        //output = HyPelToGroovyTranslator.loadGroovyFromHyPELFile(url);
    }
}
